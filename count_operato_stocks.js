db.fruits.aggregate(

  [

    {

      $match: {

        stock: {

          $gte:20

        }

      }

    },

    {

      $count: "fruits_stocks"

    }

  ]

)