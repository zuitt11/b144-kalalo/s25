db.fruits.aggregate([
   {$unwind: "$origin"},
   {$group : {_id: "$origin", sum: {$sum:1}}}
])